<?php

use Illuminate\Http\Request;

Route::group([
  'middleware'  => 'api',
  'prefix'      => 'auth',
  'namespace'   => 'Auth'
], function(){

  Route::post('register' , 'RegisterController');
  Route::post('regenerate-otp' , 'RegenerateOtpCodeController');
  Route::post('verification' , 'VerificationController');
  Route::post('update-password' , 'UpdatePasswordController');
  Route::post('login' , 'LoginController');
  Route::post('logout' , 'LogoutController')->middleware('auth:api');
  Route::post('check-token' , 'CheckTokenController')->middleware('auth:api');

  Route::get('/social/{provider}' , 'SocialiteController@redirectToProvider');
  Route::get('/social/{provider}/callback' , 'SocialiteController@handleProviderCallback');

});

Route::group([
  'middleware'  => ['api' , 'email_verified' , 'auth:api'],
], function(){

  Route::group(['prefix' => 'profile'], function(){
    Route::get('show' , 'ProfileController@show');
    Route::post('update' , 'ProfileController@update');
  });
 
});

Route::group([
  'middleware'  => 'api',
  'prefix'      => 'campaign',
], function(){
  Route::get('random/{count}' , 'CampaignController@random');
  Route::post('store' , 'CampaignController@store');
  Route::get('/' , 'CampaignController@index');
  Route::get('/{id}' , 'CampaignController@detail');
  Route::get('/search/{keyword}' , 'CampaignController@search');
});

Route::group([
  'middleware'  => 'api',
  'prefix'      => 'blog',
], function(){
  Route::get('random/{count}' , 'BlogController@random');
  Route::post('store' , 'BlogController@store');
});

Route::group([
  'middleware'  => 'api',
  'prefix'      => 'donation',
], function(){

  Route::post('/pay' , 'DonationController@pay');
  Route::post('/create' , 'DonationController@create');
  Route::post('/add-donation-detail' , 'DonationController@create');
  Route::post('/check-draft-donation' , 'DonationController@check_draft_donation');
});



