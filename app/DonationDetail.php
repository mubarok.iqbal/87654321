<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;

class DonationDetail extends Model
{
    use UsesUuid;
    
    protected $guarded = [];
}
