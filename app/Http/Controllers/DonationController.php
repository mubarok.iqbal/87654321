<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DonationDetail;
use App\Donation;
use App\User;
use Midtrans;
use DB;

class DonationController extends Controller
{
    public function pay(Request $request)
    {
        DB::beginTransaction();
        

        try {
            $donation = Donation::create([
                'user_id' => $request->user_id
            ]);
           
            $request_donation_detail = $request->donations;
           
            foreach ($request_donation_detail as $item) {
             
                DonationDetail::create([
                    'donation_id' => $donation->id,
                    'campaign_id' => $item['campaign']['id'],
                    'donation_amount' => $item['donation_amount']
                ]);
            }

            Midtrans\Config::$serverKey = config('app.midtrans.server_key');
            Midtrans\Config::$isSanitized = true;
            Midtrans\Config::$is3ds = true;

        
            $form_data = [
                "transaction_details" => [
                    "order_id" => $donation->id,
                    "gross_amount" => $request->totalDonations,
                ],         
            ];

            $respones_midtrans = Midtrans\Snap::createTransaction($form_data);

    

        } catch (\Throwable $th) {
             DB::rollback();
             return response()->json([
                'response_message' => 'gagal membuat donasi' ,
              ], 500);
        }

        DB::commit();

        return response()->json([
            'response_message' => 'berhasil' ,
            'data' => $respones_midtrans
        ], 200);


    }
       
    

    public function create(Request $request)
    {

        $donation = Donation::create([
            'user_id' => $request->user_id
        ]);

        $data['donation'] = $donation;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'donasi berhasil ditambahkan' ,
            'data'    => $data
          ], 200);
    }

    public function check_draft_donation(Request $request)
    {
        $donation = Donation::where('id' , $request->donation_id)->where('status' , 'draft')->first();

        return response()->json([
            'response_code' => '00',
            'data'    => $donation ? true : false
          ], 200);

    }

    public function add_donation_detail(Request $request)
    {
        $donataion_detail = DonationDetail::creare([
            'donation_id' => $request->donation_id,
            'campaign_id' => $request->campaign_id,
            'donation_amount' => $request->donation_amount
        ]);

        $data['donation_detail'] = $donation_detail;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'detail donasi berhasil ditambahkan' ,
            'data'    => $data
          ], 200);
    }

    
}
