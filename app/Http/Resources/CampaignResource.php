<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CampaignResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'response_code'  => '00',
            'response_message' => 'data campaign berhasil ditampilkan',
            'data'    => parent::toArray($request),
        ];
    }
}
