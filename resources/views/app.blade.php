<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{ url('/css/app.css') }}">
</head>
<body>

    <div id="app">
        <app></app>
    </div>
    <script src="{{ url('/js/app.js') }}"></script>
    <script src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="config('app.midtrans.client_key')"></script>
</body>
</html>