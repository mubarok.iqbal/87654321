import { each } from "jquery";

export default {
    namespaced : true ,
    state: {
        donations : [],
    },
    mutations: {
        insert : (state , payload) => {   
             
            state.donations.push(payload);
        },
        remove : (state , payload) => {
        
           state.donations = state.donations.filter(function( obj ) {
                 return obj.campaign.id !== payload.campaign.id;
            });
            console.log( state.donations)
        },
        removeAllDonation : (state , payload) => {
            state.donations = []
        },
        
    },
    actions: {
        insert : ({commit} , payload) => {          
            commit('insert', payload)
        },
        remove : ({commit} , payload) => {
            commit('remove', payload)
        },
       
    },
    getters: {
        donations   : state => state.donations,
        count       : state => state.donations.length,
        totalDonations : (state) => {
            var result = 0
            state.donations.forEach(function(obj){ 
                result += obj.donation_amount
            })
            return result
        }
    }
       
 
 
  }