import Vue from 'vue'
import router from './router.js'
import vuetify from './plugins/vuetify.js'
import store from './store.js'

import './plugins/currency.js'
import './bootstrap.js';
import App from './App.vue'

const app = new Vue({
    el: '#app',
    router,
    vuetify,
    store,
    components: {
        App
    },
});